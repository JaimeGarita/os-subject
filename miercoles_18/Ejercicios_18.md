1. Usando cat y una tuberia xclip copia el contenido de un archivo para luego pegarlo en un gedit
cat "Esto es un programa" | xclip -f


2. Usando exclusivamente xclip copia el contenido de un fichero y pegalo en un gedit

cat "Un programa" | xclip -sel clip


3. Copia con Ctl c el contenido de un gedit y pegalo en un fichero desde la terminal
xcplip-pastefile


4. Haz un alias para copiar y pegar y usa los alias que has creado para repetir los ejecicios anteriores
xclip-copyfile
